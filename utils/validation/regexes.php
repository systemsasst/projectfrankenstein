<?php

/**
* This file contains the various regular expressions to be used in validation across the system
* They will be stored in an associative array, to make access easier
*/

$validation_regex = array();
//alpha-numeric allowing '.', '-', '_', and '@'. 8-30 characters
$validation_regex['user_name'] = '/[a-z0-9\-\_\@\.]{8,30}/';
//Pasword requires 3 of 4 (uppercase, lowercase, number, special)
$validation_regex['passowrd']  = '/(?=^.{6,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*/';

?>
