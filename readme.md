# Project Frankenstein #
###### Title in progress.... ######


##Definition
Project Frankenstein is a User, Course, and Enrollment system for Continuing Education at Eastern Washington University. The system is made to supply a framework for non-credit courses. While the system will track enrollments, it will not actually handle payment. That will be done via a third-party application such as PayPal.
