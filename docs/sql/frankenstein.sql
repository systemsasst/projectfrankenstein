-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 02, 2014 at 09:44 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `frankenstein`
--
CREATE DATABASE IF NOT EXISTS `frankenstein` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci;
USE `frankenstein`;

-- --------------------------------------------------------

--
-- Table structure for table `assessment`
--

DROP TABLE IF EXISTS `assessment`;
CREATE TABLE IF NOT EXISTS `assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `to_ask` int(11) NOT NULL,
  `required` int(11) NOT NULL,
  `in_order` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assessment_section_idx` (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_course_department_idx` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment`
--

DROP TABLE IF EXISTS `enrollment`;
CREATE TABLE IF NOT EXISTS `enrollment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(36) COLLATE utf8_czech_ci NOT NULL,
  `course_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `payment_id` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `payment_recieved` datetime NOT NULL,
  `completed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_enrollment_user_idx` (`user_id`),
  KEY `fk_enrollment_course_idx` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `outcome`
--

DROP TABLE IF EXISTS `outcome`;
CREATE TABLE IF NOT EXISTS `outcome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assessment_id` int(11) NOT NULL,
  `enrollment_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `completed` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_assessmentoutcome_assessment_idx` (`assessment_id`),
  KEY `fk_assessmentoutcome_enrollment_idx` (`enrollment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL,
  `type` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `answer` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_question_questionbank_idx` (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question_bank`
--

DROP TABLE IF EXISTS `question_bank`;
CREATE TABLE IF NOT EXISTS `question_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assessment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_questionbank_assessment_idx` (`assessment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `q_multiple_options`
--

DROP TABLE IF EXISTS `q_multiple_options`;
CREATE TABLE IF NOT EXISTS `q_multiple_options` (
  `question_id` int(11) NOT NULL,
  `option` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`question_id`,`option`),
  KEY `fk_multiple_choice_option_idx` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `uuid` char(36) COLLATE utf8_czech_ci NOT NULL,
  `order` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`order`),
  UNIQUE KEY `uuid_UNIQUE` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`uuid`, `order`, `name`) VALUES
('1f61acea-74ce-11e4-8218-6cf794fc3309', 1, 'new'),
('2578bc5e-74ce-11e4-8218-6cf794fc3309', 2, 'admin');

--
-- Triggers `role`
--
DROP TRIGGER IF EXISTS `role_BINS`;
DELIMITER //
CREATE TRIGGER `role_BINS` BEFORE INSERT ON `role`
 FOR EACH ROW BEGIN
	DECLARE nuuid CHAR(36);
	SET nuuid = uuid();

		WHILE nuuid IN (SELECT uuid FROM role) DO
			SET nuuid = uuid;
		END WHILE;

	SET NEW.uuid = nuuid;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `content` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_section_course_idx` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `uuid` char(36) COLLATE utf8_czech_ci NOT NULL,
  `order` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_czech_ci NOT NULL,
  `pass` char(64) COLLATE utf8_czech_ci NOT NULL,
  `registered` datetime NOT NULL,
  `email_validated` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_czech_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `state` varchar(2) COLLATE utf8_czech_ci NOT NULL,
  `zipcode` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`order`),
  UNIQUE KEY `uuid_UNIQUE` (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uuid`, `order`, `username`, `pass`, `registered`, `email_validated`, `first_name`, `last_name`, `phone`, `address1`, `address2`, `city`, `state`, `zipcode`, `country`) VALUES
('5e7c1b9a-74c9-11e4-8218-6cf794fc3309', 1, 'user1', 'testpass', '2014-11-25 09:34:55', 0, 'First', 'Last', '5099445202', '507 N Skipworth Rd', NULL, 'Spokane Valley', 'WA', '99206', 'United States of America'),
('07c3dd0c-74cd-11e4-8218-6cf794fc3309', 2, 'user2', 'testpass', '2014-11-25 10:01:07', 0, 'First', 'Another', '1234567891', '123 W Some Pl', NULL, 'Spokane', 'WA', '99204', 'United States of America');

--
-- Triggers `user`
--
DROP TRIGGER IF EXISTS `user_BINS`;
DELIMITER //
CREATE TRIGGER `user_BINS` BEFORE INSERT ON `user`
 FOR EACH ROW BEGIN
	DECLARE nuuid CHAR(36);
	SET nuuid = uuid();

		WHILE nuuid IN (SELECT uuid FROM users) DO
			SET nuuid = uuid;
		END WHILE;

	SET NEW.uuid = nuuid, NEW.registered = NOW();
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `user_id` char(36) COLLATE utf8_czech_ci NOT NULL,
  `role_id` char(36) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_role_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assessment`
--
ALTER TABLE `assessment`
  ADD CONSTRAINT `fk_assessment_section` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `fk_course_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `enrollment`
--
ALTER TABLE `enrollment`
  ADD CONSTRAINT `fk_enrollment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_enrollment_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `outcome`
--
ALTER TABLE `outcome`
  ADD CONSTRAINT `fk_outcome_assessment` FOREIGN KEY (`assessment_id`) REFERENCES `assessment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_outcome_enrollment` FOREIGN KEY (`enrollment_id`) REFERENCES `enrollment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `fk_question_questionbank` FOREIGN KEY (`bank_id`) REFERENCES `question_bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_bank`
--
ALTER TABLE `question_bank`
  ADD CONSTRAINT `fk_questionbank_assessment` FOREIGN KEY (`assessment_id`) REFERENCES `assessment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `q_multiple_options`
--
ALTER TABLE `q_multiple_options`
  ADD CONSTRAINT `fk_multiple_choice_option` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `fk_section_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_user_role_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_role_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
