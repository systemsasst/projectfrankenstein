<?php
  /**
  * This file contains the class for the database connection
  */

  /**
  * This class extends the mysqli object, making it a singleton, allowing it to be auto-disconnected
  */
  class Database extends mysqli{
    /**
    * Instance of the database object
    */
    private static $instance = null;

    /**
    * Private constructor for the singleton
    */
    private function __construct(){
      $conn_info = $GLOBALS['app_vars']['database_connection_arguments'];
      var_dump($conn_info);
      parent::__construct($conn_info['address'], $conn_info['user'], $conn_info['pass'], $conn_info['database']);
    }

    public function __destruct(){
      $this->close();
    }

    /**
    * This method will return the database singleton
    */
    public static function GetInstance(){
      if(Database::$instance == null){
        Database::$instance = new Database();
      }

      return Database::$instance;
    }


  }
?>
