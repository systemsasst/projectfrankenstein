<?php
  /**
  * This file holds all of the configuration options for the application
  * It will also load any class files required
  * Require it at the top of the file
  */


  /***************************
  GLOBAL APPLICATION VARIABLES
  ***************************/
  //If debugging the application, shows extra information on errors
  $app_vars['debug'] = true;
  //What connection is being used for the database, defined in db_configuration.php
  $app_vars['database_connection'] = "test";
  //Base URL for the application
  $app_vars['base_url'] = 'localhost/frankenstein';
  //Root directory on the server for the application
  $app_vars['application_server_root'] = substr(getcwd(), 0, -11);

  //loading the regex's
  include $app_vars['application_server_root'] . 'utils/validation/regexes.php';
  $app_vars['regexes'] = $validation_regex;

  require_once 'db_configuration.php';
  $app_vars['database_connection_arguments'] = $dbconfig[$app_vars['database_connection']];

  //This is the version of app_vars that should be referenced
  $GLOBALS['app_vars'] = $app_vars;




  /******************
  CLASS FILE INCLUDES
  ******************/
  //Database class (extends mysqli)
  require_once $GLOBALS['app_vars']['application_server_root'] . '/classes/database/database.inc.php';

  
?>
