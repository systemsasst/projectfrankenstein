<?php

  /**
  * Configuration options for the database
  * MySQL (using mysqli)
  */

  $dbconfig = array();
  $dbconfig['default']['user'] = "myUser";
  $dbconfig['default']['pass'] = "totalysecretpassword";
  $dbconfig['default']['address'] = "localhost";
  $dbconfig['default']['database'] = "frankenstein";

  $dbconfig['test']['user'] = "root";
  $dbconfig['test']['pass'] = "";
  $dbconfig['test']['address'] = "localhost";
  $dbconfig['test']['database'] = "frankenstein";
?>
